var express = require('express');
const path = require('path');
const mustacheExpress = require('mustache-express');
const { startConf, fillConfiguration, createFolder, startMultipleConf } = require('./utils/completeDUConf');
const { getLatestDUs } = require('./utils/DUsList');
var app = express();

const options = {
    "kbName": "KB_GCR_P5F30",
    "kbLang": "EN",
    "characs": [],
    "project": "DYNAMO",
    "country": "GLOBAL"
};

app.set('views', `${__dirname}/views`);
app.set('view engine', 'mustache');
app.engine('mustache', mustacheExpress());
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ limit: '50mb', extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function(req, res) {
    getLatestDUs().then(response => {
        res.render("index", { dus: response, dusString: JSON.stringify(response) });
    })
});


app.get('/startConf', function(req, res) {
    console.log(req.query);
    var parameters = req.query.duChoice.split("&");
    options.kbName = parameters[0];
    options.country = parameters[1];
    options.kbLang = parameters[2];
    createFolder();
    startConf(options).then(result => {
        //console.log(result);
        //res.render("startConf", {result : JSON.stringify(result)});
        res.render("startConf", { result: result, resultString: JSON.stringify(result) });
    });
})



app.post('/fillConf', function(req, res) {
    //console.log(req.body);
    fillConfiguration(JSON.parse(req.body.conf), req.body.first).then(confComplete => {
        //console.log(confComplete);
        res.render("completedConf", { result: confComplete });
    });
})

app.get('/testdu', function(req, res) {
    //console.log(req.body);
    getLatestDUs().then(response => {
        res.render("testDuIndex", { dus: response, dusString: JSON.stringify(response) });
    })
})

app.get('/testConf', function(req, res) {
    console.log(req.query);
    var parameters = req.query.duChoice.split("&");
    options.kbName = parameters[0];
    options.country = parameters[1];
    options.kbLang = parameters[2];
    createFolder();
    startMultipleConf(options, req.query.nbTest).then(result => {
        res.render("testResult", { result: { confs: result }, first: result[0], resultString: JSON.stringify(result) });
    });
})

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.send('error');
});


module.exports = app;