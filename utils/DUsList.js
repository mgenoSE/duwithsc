const axios = require('axios');
const _ = require('underscore');
const { listenerCount } = require('../app');


async function getLatestDUs(){
    try{
        var DuList = await axios.get("https://selectconfig-ppr.schneider-electric.com/models/DYNAMO/latest?");
        var list = rankData(DuList.data); 
        return list;
    }catch(error){
        console.error();
    }    
}

function rankData(data) {
    var dusOffers = [];
    var offers = _.keys(_.countBy(data, function (data) { return data.project_version; }));
    offers.forEach(offer => {
        var offerDu = {};
        offerDu.name = offer;
        offerDu.dus = data.filter((du) => du.project_version == offer);
        dusOffers.push(offerDu);
    });
    return dusOffers;
}

exports.getLatestDUs = getLatestDUs;
exports.rankData = rankData;