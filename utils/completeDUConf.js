const axios = require('axios');
var fs = require('file-system');

const pprUrl = 'https://selectconfig-ppr.schneider-electric.com';
const AUTH_TOKEN = 'Bearer 266925ab4ed3b36396f3998683ff33d5';

var conf = {};
var folderName = "";

const switchboardDus = ["KB_GCR_MC7S1SSWB1_0", "KB_GCR_SM662SWB01_0", "KB_GCR_SM64ASSWB010"];

axios.defaults.baseURL = pprUrl;
axios.defaults.headers.common["Authorization"] = AUTH_TOKEN;
axios.defaults.headers.post["Content-Type"] = 'application/json';
axios.defaults.headers.post["Accept"] = 'application/json';


function createFolder() {
    var today = new Date();
    var time = today.getFullYear().toString() + (today.getMonth() + 1).toString() + today.getDate().toString() + today.getHours().toString() + today.getMinutes().toString() + today.getSeconds().toString();
    folderName = __dirname + '/traces/Conf' + time;
    fs.mkdir(folderName, (err) => {
        if (err) {
            return console.error(err);
        }
        console.log('Directory created successfully!');
    });
}


async function fillConfiguration(result, isSelectFirst) {

    if (result) {
        const group = findFirstIncompleteGroup(result);
        if (group === undefined) {
            console.log("all visible groups are complete");
            return await getConf(result.id).then(function(response) {
                fs.writeFile(folderName + '/completedConf.json', JSON.stringify(response.data), function(err) {});
                return response.data;
            }).catch(function(error) {
                console.log(error);
            })
        } else {
            console.log(group.name);
            //console.log(result.id);
            return await fillGroupChars(group, isSelectFirst, result.id);
        }
    } else {
        console.error("NO conf sent");
    }
}


async function fillGroupChars(group, isSelectFirst, confId) {
    const char = findFirstIncompleteCharacteristics(group);
    //console.log(char.name);
    if (char === undefined) {
        const completeGroup = await getConf(conf.id);
        return await fillConfiguration(completeGroup.data, isSelectFirst);

    } else {
        var charToSend = [getSelectCharacteristicObject(char, isSelectFirst)];
        //console.log(charToSend);
        const resultSelectChar = await setCharacteristic(charToSend, confId);
        return await fillConfiguration(resultSelectChar.data, isSelectFirst);
    }
}

function findFirstIncompleteGroup(result) {
    return result.characteristicGroups.find(group => !group.groupIsComplete && group.isVisible && group.hasRequiredCharacteristics);
}

function findFirstIncompleteCharacteristics(group) {
    return group.characteristics.find(char => !char.isReadOnly && char.isVisible && !char.isValueAssigned && char.isRequired);
}

function getSelectCharacteristicObject(char, isSelectFirst) {
    var charSet = {};
    charSet.id = char.id;
    charSet.bomPath = char.bomPath;
    var values = char.values;
    var max = values.length;
    var min = 0;
    if (isSelectFirst === "true") {
        charSet.value = values[0].valueId;
        //console.log(charSet.value);
    } else {
        var valueIndex = Math.floor(Math.random() * (+max - +min)) + +min;
        charSet.value = values[valueIndex].valueId;
    }
    return charSet;
}

async function startConf(options) {
    try {
        var response;
        const responseNew = await axios.post('/configurations?autoUnsetUserHiddenCstics=false', options);
        response = responseNew;
        if (switchboardDus.includes(responseNew.data.kbId)) {
            var optionSet = [{ "id": "YN_USER_VISIBLE", "value": "Y", "bomPath": "0000" }, { "id": "C_INTEGRATION_LEVEL", "value": "LEVC", "bomPath": "0000" }];
            const responseSwb = await axios.put('/configurations/' + encodeURI(responseNew.data.id) + '/characteristics?configType=Kb&autoUnsetUserHiddenCstics=false', optionSet);
            response = responseSwb;
        }
        fs.writeFile(folderName + '/newConf.json', JSON.stringify(response.data), function(err) {});
        if (response.data.characteristicGroups && response.data.characteristicGroups != []) {
            const responseGet = await getConf(response.data.id);
            conf = responseGet.data;
            return conf;
            //fillConfiguration(conf);
        } else {
            console.log("CharacteristicGroups is empty");
        }
    } catch (err) {
        console.log(err);
    }
}

async function startMultipleConf(options, nbTest) {
    var results = [];
    for (let index = 0; index < nbTest; index++) {
        console.log("Conf " + index + " started");
        var conf = await startConf(options);
        var confComplete = await fillConfiguration(conf, false);
        console.log("Conf " + index + " ended");
        //console.log(confComplete);
        results.push(confComplete);
    }
    return results;
}

async function getConf(confId) {
    try {
        return await axios.get('/configurations/' + encodeURI(confId) + '?configType=Kb');
    } catch (err) {
        console.log(err);
    }
}

async function setCharacteristic(char, confId) {
    try {
        var requestUrl = '/configurations/' + encodeURI(confId) + '/characteristics?autoUnsetUserHiddenCstics=false';
        return axios.post(requestUrl, char);
    } catch (err) {
        console.log(err);
    }
}


exports.createFolder = createFolder;
exports.startConf = startConf;
exports.fillConfiguration = fillConfiguration;
exports.startMultipleConf = startMultipleConf;